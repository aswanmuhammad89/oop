<?php
require('animal.php');
require('Ape.php');
require('Frog.php');
$sheep = new Animal("shaun");

// Soal 1.
echo "Name : " . $sheep->name . '<br>'; // "shaun"
echo "Legs : " . $sheep->legs . '<br>'; // 2
echo "Cold Blooded : " . $sheep->cold_blooded . '<br><br>'; // false

// Soal 2.
$sungokong = new Ape("kera sakti");
echo "Name : " . $sungokong->name . '<br>';
echo "Legs : " . $sungokong->legs . '<br>';
echo "Yel : ";
$sungokong->yell(); // "Auooo"
echo '<br>';

$kodok = new Frog("buduk");
echo "Name : " . $kodok->name . '<br>';
echo "Legs : " . $kodok->leg . '<br>';
echo "Jump : ";
$kodok->jump(); // "hop hop"
