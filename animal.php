<?php
class Animal
{
    public $legs = 2;
    public $name;
    public function __construct($name)
    {
        $this->name = $name;
    }
    public $cold_blooded = "false";
}
